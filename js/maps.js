$(function() {
            var plants = [{
                name: 'France',
                coords: [48.8696283, 2.2895549],
                status: 'closed',
            }];
            new jvm.Map({
                container: $('#map'),
                map: 'world_merc',
                markers: plants.map(function(h) {
                    return {
                        name: h.name,
                        latLng: h.coords
                    }
                }),
                labels: {
                    markers: {
                        render: function(index) {
                            return plants[index].name;
                        },
                        offsets: function(index) {
                            var offset = plants[index]['offsets'] || [0, 0];
                            return [offset[0] - 7, offset[1] + 3];
                        }
                    }
                },
                zoomOnScroll: false,
                series: {
                    markers: [{
                        attribute: 'image',
                        scale: {
                            'closed': 'images/map-marker.png',
                            'activeUntil2018': 'images/map-marker.png',
                            'activeUntil2022': 'images/map-marker.png'
                        },
                        values: plants.reduce(function(p, c, i) {
                            p[i] = c.status;
                            return p
                        }, {}),
                    }]
                }
            });
        });
